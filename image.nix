{ pkgs, modulesPath, lib, ... }:

{
  imports = [
    "${modulesPath}/installer/cd-dvd/installation-cd-minimal.nix"
    "${modulesPath}/installer/cd-dvd/channel.nix"
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = lib.mkForce [
      "btrfs"
      "reiserfs"
      "vfat"
      "f2fs"
      "xfs"
      "ntfs"
      "cifs"
    ];
  };

  environment.systemPackages = with pkgs; [
    git
  ];

  services.xserver = {
    layout = "us";
    xkbVariant = "colemak";
  };
  console.useXkbConfig = true;

  services.openssh = {
    enable = true;
    permitRootLogin = "yes";
  };

  networking = {
    hostName = "nixos-minimal";
    networkmanager = {
      enable = true;
      appendNameservers = [
        "9.9.9.9"
        "149.112.112.112"
        "2620:fe::fe"
        "2620:fe::9"
        "1.1.1.1"
        "1.0.0.1"
        "2606:4700:4700::1111"
        "2606:4700:4700::1001"
      ];
    };
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDO4/LsD0MJj8hl3w4mBEYiN1b0N4++I0fKje8fwzvas2fkli2w0s5GsAXE5eyTf/ksZcYGU4SIzx07K1QRnikEHLbGcWRefiBAZBbAYVEBIEiE2OJb4pfEQRFvTL5J8eRAtQjX+CwoTcRHEA/uZ/8TWQdSlq/OkfCfZOgx7K54noudkLkwO4/CqmbQvV8Bp9bxbc9Zc8LEpFOJrAJFbGUb6KDWpWdB2DhGuWVtvuQ1XOs3HJAJJo1BZCJ2Q/iieGumfCZm61AZ1vx5FyVaLxlL3rYiXJ0fRvBybACmkoAfmKCqkDNbVO78lWcPkgCftGYIpm8q0/XQGd28RnfTGiTOBLYCCQEzv5YYjSTuy8vgmIyihdsz2i387NyJbdeqgisR49TGCmlvGiWHJEgFEN5Lcrtqol2jsEA15RNghBgzZEjbMLze33iWHKTWzkzy3e/4PhyUAmWStvjpDVHoz5rGXzVxjF6QEUDDHqkK4qYxzUL/NrObw1wdSula3hsL7ds="
  ];

  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
    "repl-flake"
  ];
}
